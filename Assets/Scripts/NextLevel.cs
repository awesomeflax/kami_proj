﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevel : MonoBehaviour
{
    public void LoadNextScene()
    {
        string currentScene = SceneManager.GetActiveScene().name;

        if (int.Parse(currentScene[6].ToString()) < 4)
        {
            SceneManager.LoadScene("level_" + (int.Parse(currentScene[6].ToString()) + 1).ToString());
            System.IO.File.WriteAllText(Application.dataPath +"userprogress.txt", (int.Parse(currentScene[6].ToString()) + 1).ToString());
        }

        //Debug.Log(currentScene + " _ " + "level_" + (int.Parse(currentScene[6].ToString()) + 1).ToString());
    }
}