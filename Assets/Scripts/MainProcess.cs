﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainProcess : MonoBehaviour
{
    // поле с кнопками
    [SerializeField]
    private Transform puzzleField;

    // объект одного квадратика
    [SerializeField]
    private GameObject btn;

    public SpriteRenderer victory;
    public SpriteRenderer lost;
    public Button nextLvl;

    // меню селекторов
    [SerializeField]
    private Transform menuField;

    // 1 селектор цветаd
    [SerializeField]
    private GameObject label;

    // массив ячеек
    public List<Button> cells = new List<Button>();
    //массив ярылков с цветами
    public List<Button> labels = new List<Button>();

    // актуальная цветовая карта
    int[,] actualColorMap = new int[10, 16];

    // булевая карта для покраски
    bool[,] whatToColor;

    // выбранный цвет
    int selectedColor;

    //все цвета, которые мы используем
    public Sprite[] colors;
    public Sprite[] markers;

    public TriesManager tM;
    public GameObject restart;

    private bool readynow = true;
    private bool update_color = false;

    // загрузка карты уровней и прочего стаффа из уровня
    int[,] LoadLevel()
    {
        Scene scene = SceneManager.GetActiveScene();

        switch (scene.name)
        {
            case "level_0":
                labels = Level_Map_0.InitMenu();
                selectedColor = 1;
                return Level_Map_0.cellsCurrentColor;
            case "level_1":
                labels = Level_Map_1.InitMenu();
                selectedColor = 1;
                return Level_Map_1.cellsCurrentColor;
            case "level_2":
                labels = Level_Map_2.InitMenu();
                selectedColor = 1;
                return Level_Map_2.cellsCurrentColor;
            case "level_3":
                labels = Level_Map_3.InitMenu();
                selectedColor = 1;
                return Level_Map_3.cellsCurrentColor;
            case "level_4":
                labels = Level_Map_4.InitMenu();
                selectedColor = 1;
                return Level_Map_4.cellsCurrentColor;
            default:
                return null;
        }
    }

    //стартовая функция
    void Start()
    {
        colors = Resources.LoadAll<Sprite>("Textures/Colors");
        markers = Resources.LoadAll<Sprite>("Textures/Labels");

        actualColorMap = LoadLevel();
        tM.zero();

        AddButtons();
        InitButtons();
        AddListerens();
    }

    // проверку на победу
    void Update()
    {
        if (update_color && readynow)
            StartCoroutine(colorBox());

        if (tM.getScore() == 1)
        {
            int key = actualColorMap[0, 0];
            bool solution = true;

            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 16; j++)
                    if (actualColorMap[i, j] != key)
                        solution = false;

            if (solution || !update_color)
            {
                if (solution)
                {
                    var color_vic = victory.color;
                    color_vic.a += 0.8f * Time.deltaTime;
                    color_vic.a = Mathf.Clamp(color_vic.a, 0, 1);
                    victory.color = color_vic;

                    var color_next = nextLvl.GetComponent<Image>().color;
                    color_next.a += 0.8f * Time.deltaTime;
                    color_next.a = Mathf.Clamp(color_vic.a, 0, 1);
                    nextLvl.GetComponent<Image>().color = color_next;
                }
                else
                {
                    var color_lose = lost.color;
                    color_lose.a += 0.8f * Time.deltaTime;
                    color_lose.a = Mathf.Clamp(color_lose.a, 0, 1);
                    lost.color = color_lose;
                }
            }
        }
    }

    void AddButtons()
    {
        for (int i = 0; i < 160; i++)
        {
            GameObject button = Instantiate(btn);
            button.name = "" + i;
            button.transform.SetParent(puzzleField, false);
        }
    }

    private IEnumerator colorBox()
    {
        System.Random rnd = new System.Random();

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                if (whatToColor[i, j])
                {
                    readynow = false;
                    int randomColor = rnd.Next(0, 3);
                    cells[i * 16 + j].image.sprite = colors[selectedColor * 3 + randomColor];
                    actualColorMap[i, j] = selectedColor;
                    whatToColor[i, j] = false;

                    yield return new WaitForSeconds(0.03f);

                    readynow = true;
                }
            }
        }

        update_color = false;
    }

    // создание кнопок и из инициализация
    void InitButtons()
    {
        System.Random rnd = new System.Random();
        GameObject[] objects = GameObject.FindGameObjectsWithTag("PuzzleButton");

        for (int i = 0; i < objects.Length; i++)
        {
            cells.Add(objects[i].GetComponent<Button>());
        }

        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 16; j++)
            {
                int randomColor = rnd.Next(0, 3);
                cells[i * 16 + j].image.sprite = colors[actualColorMap[i, j] * 3 + randomColor];
            }
    }

    // установка слушателей
    void AddListerens()
    {
        foreach (Button btn in cells)
        {
            btn.onClick.AddListener(() => onCellClick());
        }

        foreach (Button lbl in labels)
        {
            lbl.onClick.AddListener(() => colorSelect());
        }
    }

    // выбор цвета относительно нажатой кнопки
    void colorSelect()
    {
        string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;

        switch (name)
        {
            case "lblGreen":
                if (!update_color)
                    selectedColor = 0;
                break;

            case "lblRed":
                if (!update_color)
                    selectedColor = 1;
                break;

            case "lblTeal":
                if (!update_color)
                    selectedColor = 2;
                break;

            case "lblPurple":
                if (!update_color)
                    selectedColor = 3;
                break;
        }
    }

    // обработчик кнопки
    void onCellClick()
    {
        if (victory.color.a == 0 && lost.color.a == 0)
        {
            tM.incScore();

            whatToColor = new bool[10, 16];

            // очищение поля
            for (int i = 0; i < 10; i++)
                for (int j = 0; j < 16; j++)
                    whatToColor[i, j] = false;

            // берем номер кнопки
            string name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;

            // преобразование индексов
            int indexI = int.Parse(name) / 16;
            int indexJ = int.Parse(name) % 16;

            //рекурсивная закраска
            coloringFunc(indexI, indexJ);

            update_color = true;
        }
    }

    //рекурсивная закраска
    void coloringFunc(int i, int j)
    {
        whatToColor[i, j] = true;

        if (i > 0 && actualColorMap[i - 1, j] == actualColorMap[i, j] && !whatToColor[i - 1, j])
        {
            coloringFunc(i - 1, j);
        }

        if (i < 9 && actualColorMap[i + 1, j] == actualColorMap[i, j] && !whatToColor[i + 1, j])
        {
            coloringFunc(i + 1, j);
        }

        if (j > 0 && actualColorMap[i, j - 1] == actualColorMap[i, j] && !whatToColor[i, j - 1])
        {
            coloringFunc(i, j - 1);
        }

        if (j < 15 && actualColorMap[i, j + 1] == actualColorMap[i, j] && !whatToColor[i, j + 1])
        {
            coloringFunc(i, j + 1);
        }
    }
}
