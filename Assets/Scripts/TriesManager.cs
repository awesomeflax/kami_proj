﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TriesManager : MonoBehaviour
{
    // попыток максимально/уже совершено
    int maxCountOfTries = 0;
    int curCountOfTries = 0;

    [SerializeField]
    Text tries;

    // при создании словить текст и получить попытки
    void Awake()
    {
        LoadLevel();
    }

    // получить максимум попыток
    void LoadLevel()
    {
        Scene scene = SceneManager.GetActiveScene();

        switch (scene.name)
        {
            case "level_0":
                maxCountOfTries = Level_Map_0.tries;
                break;
            case "level_1":
                maxCountOfTries = Level_Map_1.tries;
                break;
            case "level_2":
                maxCountOfTries = Level_Map_2.tries;
                break;
            case "level_3":
                maxCountOfTries = Level_Map_3.tries;
                break;
            case "level_4":
                maxCountOfTries = Level_Map_4.tries;
                break;
        }
    }

    public void incScore()
    {
        curCountOfTries++;
        tries.text = curCountOfTries.ToString() + "/" + maxCountOfTries.ToString();
    }

    public void zero()
    {
        curCountOfTries = 0;
        tries.text = curCountOfTries.ToString() + "/" + maxCountOfTries.ToString();
    }

    public int getScore()
    {
        return curCountOfTries / maxCountOfTries;
    }
}
