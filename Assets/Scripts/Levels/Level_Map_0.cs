﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level_Map_0 : MonoBehaviour
{
    // карта
    public static int[,] cellsCurrentColor = new int[10, 16];

    static public int tries = 1;

    [SerializeField]
    private Transform menuField;

    [SerializeField]
    private GameObject label;

    // что сделать при активации
    void Awake()
    {
        ColorIt();
        AddMenu();
    }

    // сгенерировать меню
    void AddMenu()
    {
        GameObject button_0 = Instantiate(label);
        button_0.name = "lblRed";
        button_0.transform.SetParent(menuField, false);

        GameObject button_1 = Instantiate(label);
        button_1.name = "lblTeal";
        button_1.transform.SetParent(menuField, false);
    }

    // заполнить меню текстурами
    static public List<Button> InitMenu()
    {
        List<Button> labels = new List<Button>();
        GameObject[] objects = GameObject.FindGameObjectsWithTag("LabelButton");
        Sprite[] markers = Resources.LoadAll<Sprite>("Textures/Labels");

        labels.Add(objects[0].GetComponent<Button>());
        labels.Add(objects[1].GetComponent<Button>());

        labels[0].image.sprite = markers[1];
        labels[1].image.sprite = markers[2];

        return labels;
    }

    // красим карту
    void ColorIt()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 16; j++)
            {
                cellsCurrentColor[i, j] = 1;
            }
        }

        for (int i = 2; i < 8; i++)
        {
            for (int j = 2; j < 14; j++)
            {
                cellsCurrentColor[i, j] = 2;
            }
        }
    }
}
