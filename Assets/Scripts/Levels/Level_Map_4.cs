﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level_Map_4 : MonoBehaviour
{
	// карта
	public static int[,] cellsCurrentColor = new int[10, 16];

	static public int tries = 3;

	[SerializeField]
	private Transform menuField;

	[SerializeField]
	private GameObject label;

	// что сделать при активации
	void Awake()
	{
		ColorIt();
		AddMenu();
	}

	// сгенерировать меню
	void AddMenu()
	{
		GameObject button_0 = Instantiate(label);
		button_0.name = "lblTeal";
		button_0.transform.SetParent(menuField, false);

		GameObject button_1 = Instantiate(label);
		button_1.name = "lblRed";
		button_1.transform.SetParent(menuField, false);

		GameObject button_2 = Instantiate(label);
		button_2.name = "lblPurple";
		button_2.transform.SetParent(menuField, false);
	}

	// заполнить меню текстурами
	static public List<Button> InitMenu()
	{
		List<Button> labels = new List<Button>();
		GameObject[] objects = GameObject.FindGameObjectsWithTag("LabelButton");
		Sprite[] markers = Resources.LoadAll<Sprite>("Textures/Labels");

		labels.Add(objects[0].GetComponent<Button>());
		labels.Add(objects[1].GetComponent<Button>());
		labels.Add(objects[2].GetComponent<Button>());

		labels[0].image.sprite = markers[2];
		labels[1].image.sprite = markers[1];
		labels[2].image.sprite = markers[3];

		return labels;
	}

	// красим карту
	void ColorIt()
	{
		// зміна порядку циклів чревата последствиями!

		for(int i = 0; i < 2; i++)				// по вертикалі
		{
			for(int j = 0; j < 3; j++)			// по горизонталі
			{
				cellsCurrentColor[i, j] = 2;
			}
		}

		for(int i = 0; i < 3; i++)
		{
			for(int j = 3; j < 9; j++)
			{
				cellsCurrentColor[i, j] = 3;
			}
		}

		for(int i = 2; i < 5; i++)
		{
			for(int j = 0; j < 9; j++)
			{
				cellsCurrentColor[i, j] = 3;
			}
		}

		for(int i = 2; i < 4; i++)
		{
			for(int j = 4; j < 7; j++)
			{
				cellsCurrentColor[i, j] = 1;
			}
		}

		for(int i = 4; i < 5; i++)
		{
			for(int j = 9; j < 16; j++)
			{
				cellsCurrentColor[i, j] = 3;
			}
		}

		for(int i = 5; i < 10; i++)
		{
			for(int j = 15; j < 16; j++)
			{
				cellsCurrentColor[i, j] = 3;
			}
		}

		for(int i = 0; i < 2; i++)
		{
			for(int j = 12; j < 16; j++)
			{
				cellsCurrentColor[i, j] = 1;
			}
		}

		for(int i = 0; i < 2; i++)
		{
			for(int j = 9; j < 12; j++)
			{
				cellsCurrentColor[i, j] = 2;
			}
		}

		for(int i = 2; i < 4; i++)
		{
			for(int j = 9; j < 12; j++)
			{
				cellsCurrentColor[i, j] = 1;
			}
		}

		for(int i = 2; i < 4; i++)
		{
			for(int j = 12; j < 16; j++)
			{
				cellsCurrentColor[i, j] = 2;
			}
		}

		for(int i = 5; i < 9; i++)
		{
			for(int j = 0; j < 5; j++)
			{
				cellsCurrentColor[i, j] = 2;
			}
		}

		for(int i = 8; i < 10; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				cellsCurrentColor[i, j] = 1;
			}
		}

		for(int i = 9; i < 10; i++)
		{
			for(int j = 4; j < 9; j++)
			{
				cellsCurrentColor[i, j] = 1;
			}
		}

		for(int i = 7; i < 9; i++)
		{
			for(int j = 5; j < 8; j++)
			{
				cellsCurrentColor[i, j] = 3;
			}
		}

		for(int i = 5; i < 7; i++)
		{
			for(int j = 5; j < 12; j++)
			{
				cellsCurrentColor[i, j] = 1;
			}
		}

		for(int i = 5; i < 7; i++)
		{
			for(int j = 12; j < 15; j++)
			{
				cellsCurrentColor[i, j] = 2;
			}
		}

		for(int i = 7; i < 9; i++)
		{
			for(int j = 12; j < 15; j++)
			{
				cellsCurrentColor[i, j] = 1;
			}
		}

		for(int i = 9; i < 10; i++)
		{
			for(int j = 9; j < 16; j++)
			{
				cellsCurrentColor[i, j] = 3;
			}
		}

		for(int i = 7; i < 9; i++)
		{
			for(int j = 8; j < 12; j++)
			{
				cellsCurrentColor[i, j] = 2;
			}
		}

		for(int i = 8; i < 10; i++)
		{
			for(int j = 9; j < 12; j++)
			{
				cellsCurrentColor[i, j] = 3;
			}
		}
	}
}