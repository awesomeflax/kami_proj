﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    private AssetBundle myLoadedAssetBundle;
    private string[] scenePaths;

    void Start()
    {
        string userProgress;
        if (System.IO.File.Exists(Application.dataPath + "userprogress.txt"))
            userProgress = System.IO.File.ReadAllText(Application.dataPath + "userprogress.txt");
        else
        {
            System.IO.File.WriteAllText(Application.dataPath + "userprogress.txt", "0");
            userProgress = System.IO.File.ReadAllText(Application.dataPath + "userprogress.txt");
        }

        List<Button> levels = new List<Button>();
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Levels");

        for (int i = 1; i < int.Parse(userProgress) + 2; i++)
        {
            GameObject object_level = GameObject.Find("load level " + i.ToString());
            object_level.GetComponent<Button>().interactable = true;
        }
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
